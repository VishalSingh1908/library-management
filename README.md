#Library Management Service

Spring Boot Application running on Java 11. It uses MongoDB Cloud as a Database.

##Development

###Run Service

    To run the service locally
    1) mvn clean package
    2) java -jar target/library-management-1.0.0.jar

    Once the service is running you can access this service using Swagger UI.

###Swagger UI

    http://localhost:8080/swagger-ui/#/library

###MongoDB Access

Paste the below mentioned connection string in Mongo DB compass to access the Database  
    
    mongodb+srv://wish:librarymanagement@library-management.cwvn0.mongodb.net/LibraryManagement

###Bitbucket

    The link to Bitbucket for Git commits
    https://bitbucket.org/VishalSingh1908/library-management/src/master/



package singh.vishal.librarymanagement.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import singh.vishal.librarymanagement.TestHelper;
import singh.vishal.librarymanagement.exception.LibraryManagementException;
import singh.vishal.librarymanagement.model.Book;
import singh.vishal.librarymanagement.repository.BookRepository;
import singh.vishal.librarymanagement.util.Constants;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class LibraryServiceImplTest {

  @InjectMocks private LibraryServiceImpl libraryService;

  @Mock private BookRepository bookRepository;

  private TestHelper testHelper;

  @BeforeEach
  void setUp() {
    testHelper = new TestHelper();
  }

  @Test
  @DisplayName("Books are present in library")
  void getListOfBooksPresentInLibraryTest1() throws LibraryManagementException {
    List<Book> books = new ArrayList<>();
    Book book =
        (Book)
            testHelper.readJavaObjectFromJsonFile(
                "/mocks/getAllTheListOfBooksPresentInLibrary/JavaBook.json", Book.class);
    books.add(book);
    Mockito.when(bookRepository.findAll()).thenReturn(books);
    List<Book> bookList = libraryService.getListOfBooksPresentInLibrary("1908");
    assertEquals("CS001", bookList.get(0).getBookId());
    assertEquals("Head First Java", bookList.get(0).getTitle());
    assertEquals("O'REILLY", bookList.get(0).getPublisher());
    assertEquals("ENG", bookList.get(0).getLanguageISOAlpha3Code());
    assertEquals("2003", bookList.get(0).getYearOfPublishing());
    assertEquals("Bert Bates", bookList.get(0).getAuthors().get(0));
    assertEquals("Kathy Sierra", bookList.get(0).getAuthors().get(1));
  }

  @Test
  @DisplayName("Empty Library")
  void getListOfBooksPresentInLibraryTest2() throws LibraryManagementException {
    List<Book> books = new ArrayList<>();
    Mockito.when(bookRepository.findAll()).thenReturn(books);
    LibraryManagementException exception =
        assertThrows(
            LibraryManagementException.class,
            () -> libraryService.getListOfBooksPresentInLibrary("1908"));
    assertEquals(Constants.BUSINESS_ERROR_CODE_1, exception.getErrorCode());
    assertEquals(Constants.BUSINESS_ERROR_MESSAGE_1, exception.getErrorMessage());
  }
}

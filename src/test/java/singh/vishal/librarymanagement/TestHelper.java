package singh.vishal.librarymanagement;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import singh.vishal.librarymanagement.exception.LibraryManagementException;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class TestHelper {

  private static final Logger LOG = LogManager.getLogger(TestHelper.class);

  public Object readJavaObjectFromJsonFile(@NotNull String jsonFilePath,@NotNull Class className) throws LibraryManagementException {
    try{
      ObjectMapper objectMapper = new ObjectMapper();
      objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
      InputStream inputStream = className.getResourceAsStream(jsonFilePath);
      byte[] json = IOUtils.toByteArray(inputStream);
      return objectMapper.readValue(json, className);
    } catch (IOException e) {
      LOG.error("Error creating java object from Json File", e);
      throw new LibraryManagementException("Error creating java object from Json File");
    }
  }

  public String createStringFromJavaObject(Object javaObject) throws JsonProcessingException {
    ObjectMapper objectMapper = new ObjectMapper();
    ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
    return objectWriter.writeValueAsString(javaObject);
  }
}

package singh.vishal.librarymanagement.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import singh.vishal.librarymanagement.TestHelper;
import singh.vishal.librarymanagement.exception.LibraryManagementException;
import singh.vishal.librarymanagement.model.Book;
import singh.vishal.librarymanagement.model.Error;
import singh.vishal.librarymanagement.model.Order;
import singh.vishal.librarymanagement.model.OrderStatus;
import singh.vishal.librarymanagement.service.LibraryService;
import singh.vishal.librarymanagement.util.Constants;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static singh.vishal.librarymanagement.util.Constants.GET_ALL_BOOKS_PRESENT;

@ExtendWith(MockitoExtension.class)
class LibraryManagementControllerTest {

  private MockMvc mockMvc;

  private TestHelper testHelper;

  @InjectMocks private LibraryManagementController libraryManagementController;

  @Mock private LibraryService libraryService;

  @BeforeEach
  void setUp() {
    mockMvc = MockMvcBuilders.standaloneSetup(libraryManagementController).build();
    testHelper = new TestHelper();
  }

  @ParameterizedTest
  @ValueSource(strings = {"NoBooksFound.json", "InternalServerError.json"})
  @DisplayName("No Data Found and Internal Server Error Test case for GetAllBooksPresentApi")
  void getAllBooksPresentTest1(String fileName) throws Exception {
    String jsonFilePath = "/mocks/getAllTheListOfBooksPresentInLibrary/" + fileName;
    Error error = (Error) testHelper.readJavaObjectFromJsonFile(jsonFilePath, Error.class);
    Mockito.when(libraryService.getListOfBooksPresentInLibrary(Mockito.anyString()))
        .thenThrow(new LibraryManagementException(error.getCode(), error.getMessage()));
    int httpStatus = error.getCode().startsWith("BE") ? 404 : 500;
    mockMvc.perform(get(GET_ALL_BOOKS_PRESENT)).andExpect(status().is(httpStatus));
  }

  @Test
  @DisplayName("Books present in the Library")
  void getAllBooksPresentTest2() throws Exception {
    Book book =
        (Book)
            testHelper.readJavaObjectFromJsonFile(
                "/mocks/getAllTheListOfBooksPresentInLibrary/JavaBook.json", Book.class);
    List<Book> bookList = new ArrayList<>();
    bookList.add(book);
    Mockito.when(libraryService.getListOfBooksPresentInLibrary(Mockito.anyString()))
        .thenReturn(bookList);
    mockMvc.perform(get(GET_ALL_BOOKS_PRESENT)).andExpect(status().isOk());
  }

  @Test
  @DisplayName("Books present in the Library")
  void userBorrowBooksPostTest1() throws Exception {
    Order order =
        (Order)
            testHelper.readJavaObjectFromJsonFile(
                "/mocks/userBorrowBooksPost/OrderRequest.json", Order.class);
    String orderJsonData = testHelper.createStringFromJavaObject(order);
    OrderStatus status =
        (OrderStatus)
            testHelper.readJavaObjectFromJsonFile(
                "/mocks/userBorrowBooksPost/SuccessfulOrder.json", OrderStatus.class);
    Mockito.when(libraryService.orchestrateBorrowingOfBooks(Mockito.any(), Mockito.anyString()))
        .thenReturn(status);
    mockMvc
        .perform(
            post(Constants.USER_BORROW_BOOKS)
                .content(orderJsonData)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.status").value("The books borrowed are CS001."));
  }

  @ParameterizedTest
  @ValueSource(strings = {"BorrowLimitExceeds.json", "LimitOfOneBook.json"})
  @DisplayName("No Data Found and Internal Server Error Test case for GetAllBooksPresentApi")
  void userBorrowBooksPostTest2(String fileName) throws Exception {
    Order order =
        (Order)
            testHelper.readJavaObjectFromJsonFile(
                "/mocks/userBorrowBooksPost/OrderRequest.json", Order.class);
    String orderJsonData = testHelper.createStringFromJavaObject(order);
    String jsonFilePath = "/mocks/userBorrowBooksPost/" + fileName;
    Error error = (Error) testHelper.readJavaObjectFromJsonFile(jsonFilePath, Error.class);
    System.out.println("error = " + error);
    Mockito.when(libraryService.orchestrateBorrowingOfBooks(Mockito.any(), Mockito.anyString()))
        .thenThrow(new LibraryManagementException(error.getCode(), error.getMessage()));
    int httpStatus = error.getCode().matches(Constants.BUSINESS_ERROR_CODE_2) ? 403 : 406;
    mockMvc
        .perform(
            post(Constants.USER_BORROW_BOOKS)
                .content(orderJsonData)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().is(httpStatus));
  }

  @Test
  @DisplayName("Books present in the Library")
  void userReturnBooksPostTest1() throws Exception {
    Order order =
        (Order)
            testHelper.readJavaObjectFromJsonFile(
                "/mocks/userReturnBooksPost/OrderRequest.json", Order.class);
    String orderJsonData = testHelper.createStringFromJavaObject(order);
    OrderStatus status =
        (OrderStatus)
            testHelper.readJavaObjectFromJsonFile(
                "/mocks/userReturnBooksPost/SuccessfulOrder.json", OrderStatus.class);
    Mockito.when(libraryService.orchestrateReturningOfBooks(Mockito.any(), Mockito.anyString()))
        .thenReturn(status);
    mockMvc
        .perform(
            post(Constants.USER_RETURN_BOOKS)
                .content(orderJsonData)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.status").value("The books returned are CS001 and CS002."));
  }

  @ParameterizedTest
  @ValueSource(strings = {"NotAcceptableBook.json", "InternalServerError.json"})
  @DisplayName("No Data Found and Internal Server Error Test case for GetAllBooksPresentApi")
  void userReturnBooksPostTest2(String fileName) throws Exception {
    Order order =
        (Order)
            testHelper.readJavaObjectFromJsonFile(
                "/mocks/userReturnBooksPost/OrderRequest.json", Order.class);
    String orderJsonData = testHelper.createStringFromJavaObject(order);
    String jsonFilePath = "/mocks/userReturnBooksPost/" + fileName;
    Error error = (Error) testHelper.readJavaObjectFromJsonFile(jsonFilePath, Error.class);
    Mockito.when(libraryService.orchestrateReturningOfBooks(Mockito.any(), Mockito.anyString()))
        .thenThrow(new LibraryManagementException(error.getCode(), error.getMessage()));
    int httpStatus = error.getCode().matches(Constants.BUSINESS_ERROR_CODE_4) ? 406 : 500;
    mockMvc
        .perform(
            post(Constants.USER_RETURN_BOOKS)
                .content(orderJsonData)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().is(httpStatus));
  }
}

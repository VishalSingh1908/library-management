package singh.vishal.librarymanagement.exception;

public class LibraryManagementException extends Exception{

    private final String errorCode;
    private final String errorMessage;
    private final String ecId;

    public LibraryManagementException(String errorCode, String errorMessage, String ecId) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.ecId = ecId;
    }

    public LibraryManagementException(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.ecId = null;
    }

    public LibraryManagementException(String errorMessage) {
        this.errorCode = null;
        this.errorMessage = errorMessage;
        this.ecId = null;
    }

    @Override
    public String toString() {
        return "LibraryManagementException{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", ecId='" + ecId + '\'' +
                '}';
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getEcId() {
        return ecId;
    }
}

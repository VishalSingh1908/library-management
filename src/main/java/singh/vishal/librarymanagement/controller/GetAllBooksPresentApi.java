package singh.vishal.librarymanagement.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import singh.vishal.librarymanagement.exception.LibraryManagementException;
import singh.vishal.librarymanagement.model.Book;
import singh.vishal.librarymanagement.model.Error;
import singh.vishal.librarymanagement.util.Constants;

import java.util.List;

@Validated
@Api(
    value = "getAllBooksPresent",
    tags = {Constants.LIBRARY})
@RequestMapping(value = "")
public interface GetAllBooksPresentApi {

  @ApiOperation(
      value = "Returns all books present in Library",
      nickname = "getAllBooksPresentGet",
      notes =
          "Returns all the books present in Library. So that the user can choose which book to borrow.<br/> If there are no books in the library, the user will get an empty list.",
      response = Book.class,
      responseContainer = "List",
      tags = Constants.LIBRARY)
  @ApiResponses(
      value = {
        @ApiResponse(
            code = 200,
            message = "successful operation",
            response = Book.class,
            responseContainer = "List"),
        @ApiResponse(code = 404, message = "No books found in the library", response = Error.class),
        @ApiResponse(
            code = 500,
            message = "Technical error occured while fetching the list",
            response = Error.class)
      })
  @GetMapping(value = Constants.GET_ALL_BOOKS_PRESENT, produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<List<Book>> getAllTheListOfBooksPresentInLibrary()
      throws LibraryManagementException;
}

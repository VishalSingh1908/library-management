package singh.vishal.librarymanagement.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import singh.vishal.librarymanagement.exception.LibraryManagementException;
import singh.vishal.librarymanagement.model.Book;
import singh.vishal.librarymanagement.model.Error;
import singh.vishal.librarymanagement.model.Order;
import singh.vishal.librarymanagement.model.OrderStatus;
import singh.vishal.librarymanagement.service.LibraryService;
import singh.vishal.librarymanagement.util.Constants;

import java.util.List;

import static singh.vishal.librarymanagement.util.Constants.*;

@RestController
@RestControllerAdvice
public class LibraryManagementController implements GetAllBooksPresentApi, UserApi {

  private static final Logger LOG = LogManager.getLogger(LibraryManagementController.class);

  private final LibraryService libraryService;

  @Autowired
  public LibraryManagementController(LibraryService libraryService) {
    this.libraryService = libraryService;
  }

  @Override
  public ResponseEntity<List<Book>> getAllTheListOfBooksPresentInLibrary()
      throws LibraryManagementException {
    String ecId = Constants.ECID;
    LOG.info("{} - Request received for fetching list of books present in Library", ecId);
    List<Book> bookList = libraryService.getListOfBooksPresentInLibrary(ecId);
    return new ResponseEntity<>(bookList, HttpStatus.OK);
  }

  @Override
  public ResponseEntity<OrderStatus> userBorrowBooksPost(Order order)
      throws LibraryManagementException {
    String ecId = Constants.ECID;
    LOG.info("{} - Request received for borrowing books from Library", ecId);
    OrderStatus orderStatus = libraryService.orchestrateBorrowingOfBooks(order, ecId);
    return new ResponseEntity<>(orderStatus, HttpStatus.OK);
  }

  @Override
  public ResponseEntity<OrderStatus> userReturnBooksPost(Order order)
      throws LibraryManagementException {
    String ecId = Constants.ECID;
    LOG.info("{} - Request received for returning books from Library", ecId);
    OrderStatus orderStatus = libraryService.orchestrateReturningOfBooks(order, ecId);
    return new ResponseEntity<>(orderStatus, HttpStatus.OK);
  }

  @ExceptionHandler(LibraryManagementException.class)
  private ResponseEntity<Error> handleLibraryManagementException(LibraryManagementException lme) {
    LOG.error("{} - Error occured - {}", lme.getEcId(), lme);
    Error errorResponse = new Error();
    errorResponse.setCode(lme.getErrorCode());
    errorResponse.setMessage(lme.getErrorMessage());
    return new ResponseEntity<>(errorResponse, httpCodeForErrors(lme.getErrorCode()));
  }

  @ExceptionHandler(Exception.class)
  private ResponseEntity<Error> handleGenericException(LibraryManagementException lme) {
    LOG.error("Unknown exception occured");
    Error errorResponse = new Error();
    errorResponse.setCode(TECHNICAL_ERROR_CODE_1);
    errorResponse.setMessage(TECHNICAL_ERROR_MESSAGE_1);
    return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  private HttpStatus httpCodeForErrors(String errorCode) {
    switch (errorCode) {
      case BUSINESS_ERROR_CODE_1:
        return HttpStatus.NOT_FOUND;
      case BUSINESS_ERROR_CODE_2:
      case BUSINESS_ERROR_CODE_5:
        return HttpStatus.FORBIDDEN;
      case BUSINESS_ERROR_CODE_3:
      case BUSINESS_ERROR_CODE_4:
      case BUSINESS_ERROR_CODE_7:
        return HttpStatus.NOT_ACCEPTABLE;
      case BUSINESS_ERROR_CODE_6:
        return HttpStatus.CONFLICT;
      default:
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }
  }
}

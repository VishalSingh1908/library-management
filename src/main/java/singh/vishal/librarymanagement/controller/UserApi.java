package singh.vishal.librarymanagement.controller;

import io.swagger.annotations.*;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import singh.vishal.librarymanagement.exception.LibraryManagementException;
import singh.vishal.librarymanagement.model.Error;
import singh.vishal.librarymanagement.model.Order;
import singh.vishal.librarymanagement.model.OrderStatus;
import singh.vishal.librarymanagement.util.Constants;

import javax.validation.Valid;

@Validated
@Api(
    value = "user",
    tags = {Constants.LIBRARY})
@RequestMapping(value = "")
public interface UserApi {

  @ApiOperation(
      value = "Borrow available book from Library",
      nickname = "userBorrowBooksPost",
      notes = "",
      response = OrderStatus.class,
      tags = {
        Constants.LIBRARY,
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "successful operation", response = OrderStatus.class),
        @ApiResponse(code = 403, message = "Your borrowing limit exceeds.", response = Error.class),
        @ApiResponse(
            code = 406,
            message = "As per guidlines you are only allowed to borrow 1 book at a time.",
            response = Error.class),
        @ApiResponse(
            code = 500,
            message = "Technical error occured while processing your request",
            response = Error.class)
      })
  @PostMapping(
      value = Constants.USER_BORROW_BOOKS,
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<OrderStatus> userBorrowBooksPost(
      @ApiParam(value = "Details of the books to be borrowed", required = true) @Valid @RequestBody
          Order body) throws LibraryManagementException;

  @ApiOperation(
      value = "Return books to the Library",
      nickname = "userReturnBooksPost",
      notes = "",
      response = OrderStatus.class,
      tags = {
        Constants.LIBRARY,
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "successful operation", response = OrderStatus.class),
        @ApiResponse(
            code = 406,
            message = "Returning book is not issued to you.",
            response = Error.class),
        @ApiResponse(
            code = 500,
            message = "Technical error occured while processing your request",
            response = Error.class)
      })
  @PostMapping(
      value = Constants.USER_RETURN_BOOKS,
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<OrderStatus> userReturnBooksPost(
      @ApiParam(value = "Return book details.", required = true) @Valid @RequestBody Order body) throws LibraryManagementException;
}

package singh.vishal.librarymanagement.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import singh.vishal.librarymanagement.model.Book;


@Repository
public interface BookRepository extends MongoRepository<Book, String> {

    Book findByBookId(String bookId);
}

package singh.vishal.librarymanagement.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import singh.vishal.librarymanagement.model.User;

import javax.validation.constraints.NotNull;
import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    List<User> findByUserId(@NotNull String userId);

}

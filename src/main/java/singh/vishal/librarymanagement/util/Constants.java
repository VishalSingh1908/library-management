package singh.vishal.librarymanagement.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Constants {

    private Constants() {
    }

    public static final String ECID = new SimpleDateFormat("yyyyMMddHHmmssSSS")
            .format(new Date()) + new Random().nextInt(10);
    public static final String GET_ALL_BOOKS_PRESENT = "/getAllBooksPresent";
    public static final String USER_BORROW_BOOKS = "/user/borrowBooks";
    public static final String USER_RETURN_BOOKS = "/user/returnBooks";
    public static final String LIBRARY = "library";
    public static final String BUSINESS_ERROR_CODE_1 = "BE001";
    public static final String BUSINESS_ERROR_MESSAGE_1 = "No books present in the library.";
    public static final String BUSINESS_ERROR_CODE_2 = "BE002";
    public static final String BUSINESS_ERROR_MESSAGE_2 = "Borrowing limit exceeds.";
    public static final String BUSINESS_ERROR_CODE_3 = "BE003";
    public static final String BUSINESS_ERROR_MESSAGE_3 = "Only 1 book is allowed at a time.";
    public static final String BUSINESS_ERROR_CODE_4 = "BE004";
    public static final String BUSINESS_ERROR_MESSAGE_4 = "User returning a book which is not issued to him/her.";
    public static final String BUSINESS_ERROR_CODE_5 = "BE005";
    public static final String BUSINESS_ERROR_MESSAGE_5 = "User is not present in the system.";
    public static final String BUSINESS_ERROR_CODE_6 = "BE006";
    public static final String BUSINESS_ERROR_MESSAGE_6 = "Multiple users present with same user Id. Please contact system administrator.";
    public static final String TECHNICAL_ERROR_CODE_1 = "TE001";
    public static final String TECHNICAL_ERROR_MESSAGE_1 = "Error occured while processing the request.";
    public static final String BUSINESS_ERROR_CODE_7 = "BE007";
    public static final String BUSINESS_ERROR_MESSAGE_7 = "This book is already issued by the user.";


}

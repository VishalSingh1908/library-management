package singh.vishal.librarymanagement.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/** User */
@Document(collection = "User")
@Validated
public class User {

  @Id
  @JsonProperty("_id")
  private String _id = null;

  @JsonProperty("userId")
  private String userId = null;

  @JsonProperty("firstName")
  private String firstName = null;

  @JsonProperty("middleName")
  private String middleName = null;

  @JsonProperty("lastName")
  private String lastName = null;

  @JsonProperty("email")
  private String email = null;

  @JsonProperty("phone")
  private String phone = null;

  @JsonProperty("borrowedBookId")
  @Valid
  private List<String> borrowedBookId = null;

  @JsonProperty("userType")
  private String userType = null;

  public User _id(String _id) {
    this._id = _id;
    return this;
  }

  @ApiModelProperty(value = "")
  public String get_Id() {
    return _id;
  }

  public User userId(String userId) {
    this.userId = userId;
    return this;
  }

  /**
   * Get userId
   *
   * @return userId
   */
  @ApiModelProperty(value = "")
  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public User firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

  /**
   * Get firstName
   *
   * @return firstName
   */
  @ApiModelProperty(value = "")
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public User middleName(String middleName) {
    this.middleName = middleName;
    return this;
  }

  /**
   * Get middleName
   *
   * @return middleName
   */
  @ApiModelProperty(value = "")
  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public User lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

  /**
   * Get lastName
   *
   * @return lastName
   */
  @ApiModelProperty(value = "")
  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public User email(String email) {
    this.email = email;
    return this;
  }

  /**
   * Get email
   *
   * @return email
   */
  @ApiModelProperty(value = "")
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public User phone(String phone) {
    this.phone = phone;
    return this;
  }

  /**
   * Get phone
   *
   * @return phone
   */
  @ApiModelProperty(value = "")
  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public User borrowedBookId(List<String> borrowedBookId) {
    this.borrowedBookId = borrowedBookId;
    return this;
  }

  public User addBorrowedBookIdItem(String borrowedBookIdItem) {
    if (this.borrowedBookId == null) {
      this.borrowedBookId = new ArrayList<String>();
    }
    this.borrowedBookId.add(borrowedBookIdItem);
    return this;
  }

  /**
   * Id of the Book which is already borrowed by the user.
   *
   * @return borrowedBookId
   */
  @ApiModelProperty(value = "Id of the Book which is already borrowed by the user.")
  public List<String> getBorrowedBookId() {
    return borrowedBookId;
  }

  public void setBorrowedBookId(List<String> borrowedBookId) {
    this.borrowedBookId = borrowedBookId;
  }

  public User userType(String userType) {
    this.userType = userType;
    return this;
  }

  /**
   * Get userType
   *
   * @return userType
   */
  @ApiModelProperty(value = "")
  public String getUserType() {
    return userType;
  }

  public void setUserType(String userType) {
    this.userType = userType;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    User user = (User) o;
    return Objects.equals(this.userId, user.userId)
        && Objects.equals(this.firstName, user.firstName)
        && Objects.equals(this.middleName, user.middleName)
        && Objects.equals(this.lastName, user.lastName)
        && Objects.equals(this.email, user.email)
        && Objects.equals(this.phone, user.phone)
        && Objects.equals(this.borrowedBookId, user.borrowedBookId)
        && Objects.equals(this.userType, user.userType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        userId, firstName, middleName, lastName, email, phone, borrowedBookId, userType);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class User {\n");

    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    middleName: ").append(toIndentedString(middleName)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
    sb.append("    borrowedBookId: ").append(toIndentedString(borrowedBookId)).append("\n");
    sb.append("    userType: ").append(toIndentedString(userType)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

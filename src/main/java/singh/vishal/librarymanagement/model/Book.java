package singh.vishal.librarymanagement.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Book
 */
@Validated
@Document(collection = "Book")
public class Book {

    @Id
    @JsonProperty("_id")
    private String _id = null;

    @JsonProperty("bookId")
    private String bookId = null;

    @JsonProperty("title")
    private String title = null;

    @JsonProperty("authors")
    @Valid
    private List<String> authors = null;

    @JsonProperty("publisher")
    private String publisher = null;

    @JsonProperty("yearOfPublishing")
    private String yearOfPublishing = null;

    @JsonProperty("languageISOAlpha3Code")
    private String languageISOAlpha3Code = null;

    @JsonProperty("availableQuantity")
    private int availableQuantity;

    @JsonProperty("borrowedQuantity")
    private int borrowedQuantity;

    public Book _id(String _id) {
        this._id = _id;
        return this;
    }

    @ApiModelProperty(value = "")
    public String get_Id() {
        return _id;
    }


    public Book bookId(String bookId) {
        this.bookId = bookId;
        return this;
    }

    /**
     * Unique Id for a book
     *
     * @return bookId
     **/
    @ApiModelProperty(value = "Unique Id for a book")


    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public Book title(String title) {
        this.title = title;
        return this;
    }

    /**
     * Get title
     *
     * @return title
     **/
    @ApiModelProperty(value = "")


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Book authors(List<String> authors) {
        this.authors = authors;
        return this;
    }

    public Book addAuthorsItem(String authorsItem) {
        if (this.authors == null) {
            this.authors = new ArrayList<>();
        }
        this.authors.add(authorsItem);
        return this;
    }

    /**
     * List of Author.
     *
     * @return authors
     **/
    @ApiModelProperty(value = "List of Author.")


    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public Book publisher(String publisher) {
        this.publisher = publisher;
        return this;
    }

    /**
     * Get publisher
     *
     * @return publisher
     **/
    @ApiModelProperty(value = "")


    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Book yearOfPublishing(String yearOfPublishing) {
        this.yearOfPublishing = yearOfPublishing;
        return this;
    }

    /**
     * Get yearOfPublishing
     *
     * @return yearOfPublishing
     **/
    @ApiModelProperty(value = "")


    public String getYearOfPublishing() {
        return yearOfPublishing;
    }

    public void setYearOfPublishing(String yearOfPublishing) {
        this.yearOfPublishing = yearOfPublishing;
    }

    public Book languageISOAlpha3Code(String languageISOAlpha3Code) {
        this.languageISOAlpha3Code = languageISOAlpha3Code;
        return this;
    }

    /**
     * Get languageISOAlpha3Code
     *
     * @return languageISOAlpha3Code
     **/
    @ApiModelProperty(value = "")

    @Size(min = 3, max = 3)
    public String getLanguageISOAlpha3Code() {
        return languageISOAlpha3Code;
    }

    public void setLanguageISOAlpha3Code(String languageISOAlpha3Code) {
        this.languageISOAlpha3Code = languageISOAlpha3Code;
    }

    public Book availableQuantity(int availableQuantity) {
        this.availableQuantity = availableQuantity;
        return this;
    }

    /**
     * Get availableQuantity
     *
     * @return availableQuantity
     **/
    @ApiModelProperty(value = "")

    @Valid

    public int getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(int availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    public Book borrowedQuantity(int borrowedQuantity) {
        this.borrowedQuantity = borrowedQuantity;
        return this;
    }

    /**
     * Get borrowedQuantity
     *
     * @return borrowedQuantity
     **/
    @ApiModelProperty(value = "")

    @Valid

    public int getBorrowedQuantity() {
        return borrowedQuantity;
    }

    public void setBorrowedQuantity(int borrowedQuantity) {
        this.borrowedQuantity = borrowedQuantity;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Book book = (Book) o;
        return Objects.equals(this.bookId, book.bookId) &&
                Objects.equals(this.title, book.title) &&
                Objects.equals(this.authors, book.authors) &&
                Objects.equals(this.publisher, book.publisher) &&
                Objects.equals(this.yearOfPublishing, book.yearOfPublishing) &&
                Objects.equals(this.languageISOAlpha3Code, book.languageISOAlpha3Code) &&
                Objects.equals(this.availableQuantity, book.availableQuantity) &&
                Objects.equals(this.borrowedQuantity, book.borrowedQuantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookId, title, authors, publisher, yearOfPublishing, languageISOAlpha3Code, availableQuantity, borrowedQuantity);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Book {\n");

        sb.append("    bookId: ").append(toIndentedString(bookId)).append("\n");
        sb.append("    title: ").append(toIndentedString(title)).append("\n");
        sb.append("    authors: ").append(toIndentedString(authors)).append("\n");
        sb.append("    publisher: ").append(toIndentedString(publisher)).append("\n");
        sb.append("    yearOfPublishing: ").append(toIndentedString(yearOfPublishing)).append("\n");
        sb.append("    languageISOAlpha3Code: ").append(toIndentedString(languageISOAlpha3Code)).append("\n");
        sb.append("    availableQuantity: ").append(toIndentedString(availableQuantity)).append("\n");
        sb.append("    borrowedQuantity: ").append(toIndentedString(borrowedQuantity)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}


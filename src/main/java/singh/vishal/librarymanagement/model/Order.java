package singh.vishal.librarymanagement.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/** Order */
@Validated
public class Order {
  @JsonProperty("userId")
  private String userId = null;

  @JsonProperty("bookIds")
  @Valid
  private List<String> bookIds = new ArrayList<String>();

  @JsonProperty("timestamp")
  private String timestamp = null;

  public Order userId(String userId) {
    this.userId = userId;
    return this;
  }

  /**
   * Get userId
   *
   * @return userId
   */
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Order bookIds(List<String> bookIds) {
    this.bookIds = bookIds;
    return this;
  }

  public Order addBookIdsItem(String bookIdsItem) {
    this.bookIds.add(bookIdsItem);
    return this;
  }

  /**
   * Id of the Book which which user wants to borrow.
   *
   * @return bookIds
   */
  @ApiModelProperty(required = true, value = "Id of the Book which which user wants to borrow.")
  @NotNull
  public List<String> getBookIds() {
    return bookIds;
  }

  public void setBookIds(List<String> bookIds) {
    this.bookIds = bookIds;
  }

  public Order timestamp(String timestamp) {
    this.timestamp = timestamp;
    return this;
  }

  /**
   * Get timestamp
   *
   * @return timestamp
   */
  @ApiModelProperty(value = "")
  @Valid
  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Order order = (Order) o;
    return Objects.equals(this.userId, order.userId)
        && Objects.equals(this.bookIds, order.bookIds)
        && Objects.equals(this.timestamp, order.timestamp);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, bookIds, timestamp);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Order {\n");

    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    bookIds: ").append(toIndentedString(bookIds)).append("\n");
    sb.append("    timestamp: ").append(toIndentedString(timestamp)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

package singh.vishal.librarymanagement.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import singh.vishal.librarymanagement.util.Constants;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerUIConfig {

  ApiInfo apiInfo() {
    return new ApiInfoBuilder()
        .title("Library Management")
        .description("Assignment for the library management")
        .version("1.0.0")
        .contact(new Contact("Vishal Singh", "", "vishal.singh1908@hotmail.com"))
        .build();
  }

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.basePackage("singh.vishal.librarymanagement.controller"))
        .paths(PathSelectors.any())
        .build()
        .tags(new Tag(Constants.LIBRARY, "the getAllBooksPresent API"))
        .apiInfo(apiInfo());
  }
}

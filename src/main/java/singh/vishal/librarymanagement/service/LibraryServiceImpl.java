package singh.vishal.librarymanagement.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import singh.vishal.librarymanagement.exception.LibraryManagementException;
import singh.vishal.librarymanagement.model.Book;
import singh.vishal.librarymanagement.model.Order;
import singh.vishal.librarymanagement.model.OrderStatus;
import singh.vishal.librarymanagement.model.User;
import singh.vishal.librarymanagement.repository.BookRepository;
import singh.vishal.librarymanagement.repository.UserRepository;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static singh.vishal.librarymanagement.util.Constants.*;

@Service
public class LibraryServiceImpl implements LibraryService {

  private static final Logger LOG = LogManager.getLogger(LibraryServiceImpl.class);

  private final BookRepository bookRepository;

  private final UserRepository userRepository;

  @Autowired
  public LibraryServiceImpl(BookRepository bookRepository, UserRepository userRepository) {
    this.bookRepository = bookRepository;
    this.userRepository = userRepository;
  }

  @Override
  public List<Book> getListOfBooksPresentInLibrary(String ecId) throws LibraryManagementException {
    List<Book> bookList = bookRepository.findAll();
    LOG.info("{} - List of Books in Library - {}", ecId, bookList);
    List<Book> availableBooks =
        bookList.stream()
            .filter(book -> book.getAvailableQuantity() > 0)
            .collect(Collectors.toList());
    LOG.info("{} - List of available Books in Library - {}", ecId, availableBooks);
    if (!availableBooks.isEmpty()) {
      return availableBooks;
    } else {
      throw new LibraryManagementException(BUSINESS_ERROR_CODE_1, BUSINESS_ERROR_MESSAGE_1, ecId);
    }
  }

  @Override
  public OrderStatus orchestrateBorrowingOfBooks(Order order, String ecId)
      throws LibraryManagementException {
    User user = checkUserDetails(order, ecId);
    validateOrderRequest(order, user, ecId);
    user.getBorrowedBookId().add(order.getBookIds().get(0));
    userRepository.save(user);
    Book book = bookRepository.findByBookId(order.getBookIds().get(0));
    book.setAvailableQuantity(book.getAvailableQuantity() - 1);
    book.setBorrowedQuantity(book.getBorrowedQuantity() + 1);
    bookRepository.save(book);
    OrderStatus orderStatus = new OrderStatus();
    orderStatus.setStatus("Successful");
    return orderStatus;
  }

  private User checkUserDetails(Order order, String ecId) throws LibraryManagementException {
    List<User> users = validateUser(order, ecId);
    if (users.get(0).getBorrowedBookId().size() > 1) {
      throw new LibraryManagementException(BUSINESS_ERROR_CODE_2, BUSINESS_ERROR_MESSAGE_2, ecId);
    }
    return users.get(0);
  }

  private List<User> validateUser(Order order, String ecId) throws LibraryManagementException {
    List<User> users = userRepository.findByUserId(order.getUserId());
    if (users == null || users.isEmpty()) {
      throw new LibraryManagementException(BUSINESS_ERROR_CODE_5, BUSINESS_ERROR_MESSAGE_5, ecId);
    } else if (users.size() > 1) {
      throw new LibraryManagementException(BUSINESS_ERROR_CODE_6, BUSINESS_ERROR_MESSAGE_6, ecId);
    }
    return users;
  }

  private void validateOrderRequest(Order order, User user, String ecId)
      throws LibraryManagementException {
    if (order.getBookIds().size() > 1) {
      throw new LibraryManagementException(BUSINESS_ERROR_CODE_3, BUSINESS_ERROR_MESSAGE_3, ecId);
    }
    if (user.getBorrowedBookId() != null && !user.getBorrowedBookId().isEmpty()) {
      for (String bookId : user.getBorrowedBookId()) {
        if (Collections.singletonList(order.getBookIds()).contains(bookId)) {
          throw new LibraryManagementException(
              BUSINESS_ERROR_CODE_7, BUSINESS_ERROR_MESSAGE_7, ecId);
        }
      }
    }
  }

  @Override
  public OrderStatus orchestrateReturningOfBooks(Order order, String ecId)
      throws LibraryManagementException {
    User user = validateUser(order, ecId).get(0);
    for (String bookId : order.getBookIds()) {
      if (user.getBorrowedBookId().contains(bookId)) {
        user.getBorrowedBookId().remove(bookId);
      } else {
        throw new LibraryManagementException(
                BUSINESS_ERROR_CODE_4, BUSINESS_ERROR_MESSAGE_4, ecId);
      }
    }
    userRepository.save(user);
    Book book = bookRepository.findByBookId(order.getBookIds().get(0));
    book.setAvailableQuantity(book.getAvailableQuantity() + 1);
    book.setBorrowedQuantity(book.getBorrowedQuantity() - 1);
    bookRepository.save(book);
    OrderStatus orderStatus = new OrderStatus();
    orderStatus.setStatus("Successful");
    return orderStatus;
  }
}

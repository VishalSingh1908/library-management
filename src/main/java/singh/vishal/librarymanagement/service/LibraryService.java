package singh.vishal.librarymanagement.service;

import singh.vishal.librarymanagement.exception.LibraryManagementException;
import singh.vishal.librarymanagement.model.Book;
import singh.vishal.librarymanagement.model.Order;
import singh.vishal.librarymanagement.model.OrderStatus;

import java.util.List;

public interface LibraryService {

    List<Book> getListOfBooksPresentInLibrary(String ecId) throws LibraryManagementException;

    OrderStatus orchestrateBorrowingOfBooks(Order order, String ecId) throws LibraryManagementException;

    OrderStatus orchestrateReturningOfBooks(Order order, String ecId) throws LibraryManagementException;

}
